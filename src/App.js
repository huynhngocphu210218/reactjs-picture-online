/*!
=========================================================
* Muse Ant Design Dashboard - v1.0.0
=========================================================
* Product Page: https://www.creative-tim.com/product/muse-ant-design-dashboard
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/muse-ant-design-dashboard/blob/Admin/LICENSE.md)
* Coded by Creative Tim
=========================================================
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/
import {
  Switch,
  Route,
  Redirect,
  useHistory,
  useLocation,
} from "react-router-dom";
import "antd/dist/antd.css";
import "./assets/styles/main.css";
import "./assets/styles/responsive.css";
import { Button, Result } from "antd";
import Home from "./pages/Home";
import Tables from "./pages/Tables";
import Billing from "./pages/Billing";
import Rtl from "./pages/Rtl";
import Profile from "./pages/Profile";
import SignUp from "./pages/SignUp";
import SignIn from "./pages/SignIn";
import Admin from "./components/layout/Admin";
import UserLayout from "./components/layout/UserLayout";
import Categories from "./pages/admin/Categories";
import Sizes from "./pages/admin/Sizes";
import Users from "./pages/admin/Users";
import Products from "./pages/admin/Products";
import UserProducts from "./pages/user/Products";
import { useEffect } from "react";
import ProductDetails from "./pages/admin/ProductDetails";
import Addtocart from "./pages/user/Cart";
import DetailProductiInCart from "./pages/user/DetailProductInCart";
import UserStore from "./stores/UserStore";
import PaymentResult from "./pages/user/PaymentResult";
import Orders from "./pages/admin/Orders";

function App() {
  const history = useHistory();
  const location = useLocation();
  useEffect(() => {
    // Callback function to handle route change
    console.log(`Route changed to: ${location.pathname}`);
  }, [location]);

  useEffect(() => {
    UserStore.loadUserStore();
  }, []);

  const _goToHome = () => {
    history.replace("/home");
  };
  return (
    <div className="App">
      <Switch>
        <Route path="/sign-up" exact component={SignUp} />
        <Route path="/sign-in" exact component={SignIn} />
        <Route
          exact
          path="/"
          component={() => (
            <UserLayout>
              <UserProducts />
            </UserLayout>
          )}
        />
        <Route
          exact
          path="/Cart"
          component={() => (
            <UserLayout>
              <Addtocart />
            </UserLayout>
          )}
        />
        <Route
          exact
          path="/DetailProductInCart"
          component={() => (
            <UserLayout>
              <DetailProductiInCart />
            </UserLayout>
          )}
        />
        <Route
          exact
          path="/PaymentResult"
          component={() => (
            <UserLayout>
              <PaymentResult />
            </UserLayout>
          )}
        />
        <Route
          exact
          path="/home"
          component={() => (
            <Admin>
              <Home />
            </Admin>
          )}
        />
        <Route
          exact
          path="/tables"
          component={() => (
            <Admin>
              <Tables />
            </Admin>
          )}
        />
        <Route
          exact
          path="/billing"
          component={() => (
            <Admin>
              <Billing />
            </Admin>
          )}
        />
        <Route exact path="/rtl" component={Rtl} />
        <Route exact path="/profile" component={Profile} />
        <Route
          path="/categories"
          component={() => (
            <Admin>
              <Categories />
            </Admin>
          )}
        />

        <Route
          path="/sizes"
          component={() => (
            <Admin>
              <Sizes />
            </Admin>
          )}
        />
        <Route
          path="/users"
          component={() => (
            <Admin>
              <Users />
            </Admin>
          )}
        />
        <Route
          exact
          path="/products"
          component={() => (
            <Admin>
              <Products />
            </Admin>
          )}
        />
        <Route
          path="/products/:id"
          component={() => (
            <Admin>
              <ProductDetails />
            </Admin>
          )}
        />
        <Route
          exact
          path="/orders"
          component={() => (
            <Admin>
              <Orders />
            </Admin>
          )}
        />
        <Route
          component={() => (
            <Result
              status="404"
              title="404"
              subTitle="Sorry, the page you visited does not exist."
              extra={
                <Button type="primary" onClick={_goToHome}>
                  Back Home
                </Button>
              }
            />
          )}
        />
      </Switch>
    </div>
  );
}

export default App;
