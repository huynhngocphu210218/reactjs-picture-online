import { Image } from "antd";
import { forwardRef, useImperativeHandle, useState } from "react";
import { DeleteOutlined, UndoOutlined } from "@ant-design/icons";
import { SERVER_BASE_URL } from "../../utils/Constants";

const ImageList = ({ images }, ref) => {
  const [deletedList, setDeletedList] = useState([]);

  useImperativeHandle(ref, () => {
    return {
      deletedList: () => {
        return deletedList;
      },
    };
  });

  return (
    <div style={{ display: "flex", flexWrap: "wrap" }}>
      {images.map((img, index) => (
        <div
          key={index}
          style={{
            position: "relative",
            marginRight: 10,
            marginBottom: 10,
          }}
        >
          <Image
            src={`${SERVER_BASE_URL}/images/${img.name}`}
            style={{
              width: 100, // Kích thước của hình ảnh
              height: 100,
              objectFit: "cover",
              opacity: deletedList.includes(img.id) ? 0.2 : 1,
            }}
            preview={!deletedList.includes(img.id)}
          />
          <div
            style={{
              position: "absolute",
              top: "0px",
              right: "0px",
              cursor: "pointer",
            }}
          >
            {deletedList.includes(img.id) ? (
              <UndoOutlined
                onClick={() =>
                  setDeletedList(deletedList.filter((_) => _ !== img.id))
                }
              />
            ) : (
              <DeleteOutlined
                onClick={() => setDeletedList([...deletedList, img.id])}
              />
            )}
          </div>
        </div>
      ))}
    </div>
  );
};

export default forwardRef(ImageList);
