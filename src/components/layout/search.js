import { AudioOutlined } from '@ant-design/icons';
import { Input, Space } from 'antd';
const { Search } = Input;
const suffix = (
  <AudioOutlined
    style={{
      fontSize: 16,
      color: '#111111',
    }}
  />
);
const onSearch = (value) => console.log(value);
const Searchsp = () => (
  <Space direction="vertical">
    <Search
      placeholder="Search"
      enterButton="Search"
      size="large"
      suffix={suffix}
      onSearch={onSearch}
    />
  </Space>
);
export default Searchsp;