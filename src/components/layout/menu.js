
import { Menu } from 'antd';
import { useState } from 'react';
const items = [
  {
    label: 'TRANH TRỪU TƯỢNG',
    key: 'picture',
  },
  {
    label: 'KHUNG TRANH',
    key: 'picture2',
  },
  {
    label: 'TRANH NỔI BẬT',
    key: 'SubMenu',
    children: [
      {
        type: 'group',
        label: 'Tranh Sơn Dầu',
        children: [
          {
            label: 'Tranh Tình Yêu',
            key: 'sd:1',
          },
          {
            label: 'Tranh Phật giáo',
            key: 'sd:2',
          },
          {
            label: 'Tranh Hoa',
            key: 'sd:3',
          },
        ],
      },
      {
        type: 'group',
        label: 'Tranh Phong Cảnh',
        children: [
          {
            label: 'Tranh Phong Cảnh Vùng Cao',
            key: 'sd:4',
          },
          {
            label: 'Tranh Phong Cảnh Đồng Quê',
            key: 'sd:5',
          },
        ],
      },
    ],
  },
//   {
//     label: (
//       <a href="" target="_blank" rel="noopener noreferrer">
//         Navigation Four - Link
//       </a>
//     ),
//     key: 'alipay',
//   },
];
const Menusp = () => {
  const [current, setCurrent] = useState('mail');
  const onClick = (e) => {
    console.log('click ', e);
    setCurrent(e.key);
  };
  return <Menu onClick={onClick} selectedKeys={[current]} mode="horizontal" items={items} />;
};
export default Menusp;