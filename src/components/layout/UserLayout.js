import { Row, Col } from "antd";
import { Pagination } from "antd";
import Searchsp from "./search";
import Menusp from "./menu";
import { ShoppingCartOutlined } from "@ant-design/icons";
import { size } from "lodash";
import Slideshow from "../../pages/user/slidershow";
import { KEY_PRODUCTS_IN_CART } from "../../utils/Constants";
import { useEffect, useRef } from "react";
import { useLocation, useHistory } from "react-router-dom";
import UserStore from "../../stores/UserStore";
import { observer } from "mobx-react";
import { NavLink } from "react-router-dom/cjs/react-router-dom.min";

// const _goToDetailINCart = (data) => {
//   history.push("/Cart", { data });
// };

const UserLayout = (props) => {
  // const location = useLocation();
  // const data = location.state.data;
  const history = useHistory();
  const _goToDetailInCart = () => {
    history.push("/DetailProductInCart");
  };
  return (
    <>
      <Row style={{ height: 45, width: "100%", backgroundColor: "pink" }}></Row>
      <Row style={{ margin: 5 }}>
        <NavLink to="/">
          <Row style={{ fontSize: 200, marginBottom: 15 }}>
            <img src="https://bantranh.com/wp-content/uploads/2019/02/logo-2.png"></img>
          </Row>
        </NavLink>
        <Row style={{ marginTop: 15, marginLeft: 150 }}>
          <Searchsp />
        </Row>
        <Row style={{ margin: 15 }}>
          <Menusp />
        </Row>
        <div
          style={{
            position: "relative",
            display: "flex",
            marginLeft: 200,
            marginTop: 15,
          }}
        >
          <ShoppingCartOutlined
            onClick={_goToDetailInCart}
            style={{ fontSize: 40 }}
          />
          <span
            style={{
              position: "absolute",
              top: -10,
              right: -10,
              padding: "0px 8px 0px 8px",
              borderRadius: 20,
              backgroundColor: "red",
              color: "white",
            }}
          >
            {UserStore.productsInCart.length}
          </span>
        </div>
      </Row>
      <Row
        style={{ height: 45, width: "100%", backgroundColor: "black" }}
      ></Row>
      <Slideshow></Slideshow>
      <Row>
        <Col span={3} />
        <Col span={18}>{props.children}</Col>
        <Col span={3} />
      </Row>

      {/* <Row justify="center">
        <Pagination defaultCurrent={1} total={50} />
      </Row> */}
    </>
  );
};

export default observer(UserLayout);
