import moment from "moment";

export const resetObject = (obj) => {
  const rs = {};
  Object.keys(obj).forEach((key) => {
    if (obj[key] instanceof Array) rs[key] = [];
    else rs[key] = "";
  });
  return rs;
};

export const toDateString = (date) => {
  return moment(date).format("YYYY-MM-DD");
};

export const getBase64 = (file) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });

export const uriToUploadFile = async (uri) => {
  try {
    const response = await fetch(uri);
    const blob = await response.blob();
    const filename = uri.split("/").pop();
    const type = blob.type;
    const file = new File([blob], filename, { type });
    return {
      uid: Math.random().toString(),
      name: filename,
      status: "done",
      url: URL.createObjectURL(file),
      thumbUrl: URL.createObjectURL(file),
    };
  } catch (error) {
    console.log(error);
    return null;
  }
};
