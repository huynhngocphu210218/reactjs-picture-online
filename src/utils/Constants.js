export const SERVER_BASE_URL = "http://localhost:1337";

export const USER_INFO = "USER_INFO";

export const KEY_PRODUCTS_IN_CART = "KEY_PRODUCTS_IN_CART";

export const KEY_USER_PAYMENT = "KEY_USER_PAYMENT";
