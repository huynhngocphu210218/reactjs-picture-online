import axios from "axios";
import { SERVER_BASE_URL } from "./Constants";

class API {
  constructor() {
    this.configs = {
      baseURL: `${SERVER_BASE_URL}/api/`,
      timeout: 60000,
    };
  }

  instance(cfgs = {}) {
    return axios.create({ ...this.configs, ...cfgs });
  }

  async get(url, data) {
    const rs = await this.instance().get(url, { params: data });
    return this.response(rs);
  }

  async post(url, data, cfgs = {}) {
    const rs = await this.instance(cfgs).post(url, data);
    return this.response(rs);
  }

  async patch(url, data) {
    const rs = await this.instance().patch(url, data);
    return this.response(rs);
  }

  async delete(url) {
    const rs = await this.instance().delete(url);
    return this.response(rs);
  }

  response(rs) {
    return rs?.data;
  }
}
const ApiInstance = new API();
export default ApiInstance;
