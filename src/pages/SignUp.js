import React from "react";
import { Layout, Button, Typography, Card, Form, Input } from "antd";
import { notification } from "antd";
import API from "../utils/API";

import { Link, useHistory } from "react-router-dom";

const { Title } = Typography;
const { Content } = Layout;

const SignUp = () => {
  const [notify, contextHolder] = notification.useNotification();
  const history = useHistory();
  const onFinish = async (values) => {
    const { username, password, email } = values;
    const rs = await API.post("signup", { username, password, email });

    if (rs.code === 200) {
      history.push("/sign-in");
    } else {
      notify.error({
        message: `Đăng ký thất bại!`,
        description: rs.status,
        placement: "topRight",
      });
    }
    console.log("kkkkk rs", rs);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <>
      <div className="layout-default ant-layout layout-sign-up">
        <Content className="p-0">
          <div className="sign-up-header">
            <div className="content">
              <Title>Sign Up</Title>
              <p className="text-lg">
                Use these awesome forms to login or create new account in your
                project for free.
              </p>
            </div>
          </div>

          <Card
            className="card-signup header-solid h-full ant-card pt-0"
            title={<h5>Register account</h5>}
            bordered="false"
          >
            <Form
              name="basic"
              initialValues={{ remember: true }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              className="row-col"
            >
              <Form.Item
                name="username"
                rules={[
                  { required: true, message: "Please input your username!" },
                ]}
              >
                <Input placeholder="Username" />
              </Form.Item>
              <Form.Item
                name="email"
                rules={[
                  { required: true, message: "Please input your email!" },
                ]}
              >
                <Input placeholder="Email" />
              </Form.Item>
              <Form.Item
                name="password"
                rules={[
                  { required: true, message: "Please input your password!" },
                ]}
              >
                <Input placeholder="Password" type="password" />
              </Form.Item>

              <Form.Item>
                <Button
                  style={{ width: "100%" }}
                  type="primary"
                  htmlType="submit"
                >
                  SIGN UP
                </Button>
              </Form.Item>
            </Form>
            <p className="font-semibold text-muted text-center">
              Already have an account?{" "}
              <Link to="/sign-in" className="font-bold text-dark">
                Sign In
              </Link>
            </p>
          </Card>
        </Content>

        {contextHolder}
      </div>
    </>
  );
};

export default SignUp;
