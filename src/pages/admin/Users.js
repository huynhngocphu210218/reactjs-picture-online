import { useEffect, useState, useRef } from "react";
import API from "../../utils/API";
import {
  notification,
  Button,
  Space,
  Modal,
  Form,
  Input,
  DatePicker,
  Table,
  Popconfirm,
  Select,
} from "antd";
import { PlusOutlined, CloseOutlined } from "@ant-design/icons";
import moment from "moment";
import { resetObject, toDateString } from "../../utils/Common";
import _ from "lodash";
const { Option } = Select;

const Users = () => {
  const formRef = useRef(null);
  const [users, setUsers] = useState([]);
  const [notify, contextHolder] = notification.useNotification();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [user, setUser] = useState({});

  useEffect(() => {
    console.log("kkkkk user effect", user);
  }, [user]);

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    console.log("kkkkk isModalOpen isEdit", isModalOpen, isEdit);
    if (isModalOpen && isEdit) {
      formRef.current?.setFieldsValue({
        ...user,
        gender: `${user.gender}`,
        dob: moment(user.dob),
      });
    } else {
      formRef.current?.setFieldsValue({
        ...resetObject(user),
        gender: "1",
        dob: moment(),
        role: "staff",
      });
    }
  }, [isModalOpen, isEdit]);

  const getData = async () => {
    const rs = await API.get("user");
    if (rs.code === 200) {
      setUsers(rs.data);
    } else {
      notify.error({
        message: `Load users failed!`,
        description: rs.status,
        placement: "topRight",
      });
    }
    console.log("kkkkk users", rs);
  };

  const showModal = () => {
    setIsModalOpen(true);
  };

  const hideModal = () => {
    setIsModalOpen(false);
  };

  // xử lý dữ liệu để có thể lưu vào csdl
  const proccessUser = () => {
    if (user.dob) {
      user.dob = toDateString(user.dob);
    } else {
      user.dob = toDateString(moment());
    }
    user.gender = user.gender ? parseInt(user.gender) : 1;
    if (!user.role) user.role = "staff";
    return user;
  };

  const onFinish = async () => {
    console.log("kkkkk user", proccessUser());
    if (isEdit) {
      const rs = await API.patch(`user/${user.id}`, user);
      console.log("kkkkk update user", rs);
      if (rs.code === 200) {
        setIsModalOpen(false);
        getData();
      } else {
        notify.error({
          message: `Lỗi sửa người dùng!`,
          description: rs.status,
          placement: "topRight",
        });
      }
    } else {
      const rs = await API.post("user", user);
      console.log("kkkkk add user", rs);
      if (rs.code === 200) {
        setIsModalOpen(false);
        getData();
      } else {
        notify.error({
          message: `Lỗi thêm người dùng!`,
          description: rs.status,
          placement: "topRight",
        });
      }
    }
  };

  const onChangeText = (key, e) => {
    console.log("kkkkk ", e?.target?.value ?? e);
    if (["dob", "gender", "role"].includes(key)) {
      setUser({ ...user, [key]: e });
    } else {
      setUser({ ...user, [key]: e.target.value });
    }
  };

  const onAdd = () => {
    setIsEdit(false);
    showModal();
  };

  const onEdit = (id) => {
    const _user = users.find((k) => k.id === id);
    setIsModalOpen(true);
    setIsEdit(true);
    setUser(_user);
    console.log("kkkkk onEdit user", _user);
  };

  const onDelete = async (id) => {
    const rs = await API.delete(`user/${id}`);
    console.log("kkkkk delete user", rs);
    if (rs.code === 200) {
      notify.success({
        message: `Xóa người dùng thành công!`,
        placement: "topRight",
      });
      getData();
    } else {
      notify.error({
        message: `Lỗi xóa người dùng!`,
        description: rs.status,
        placement: "topRight",
      });
    }
  };

  const COLUMNS = [
    {
      title: "Username",
      dataIndex: "username",
      fixed: "left",
      width: 150,
    },
    {
      title: "Email",
      dataIndex: "email",
      fixed: "left",
      width: 200,
    },
    {
      title: "Fullname",
      dataIndex: "fullName",
      width: 150,
    },
    {
      title: "Phone",
      dataIndex: "phone",
      width: 150,
    },
    {
      title: "Dob",
      dataIndex: "dob",
      width: 120,
      render: (val) => <span>{moment(val).format("DD-MM-YYYY")}</span>,
    },
    {
      title: "Gender",
      dataIndex: "gender",
      width: 100,
      render: (val) => <span>{val === 1 ? "Nam" : "Nữ"}</span>,
    },
    {
      title: "Address",
      dataIndex: "address",
      width: 200,
    },
    {
      title: "Role",
      dataIndex: "role",
      render: (val) => <span>{_.capitalize(val)}</span>,
    },
    {
      title: "Created At",
      dataIndex: "createdAt",
      render: (val) => <span>{moment(val).format("DD-MM-YYYY HH:mm:ss")}</span>,
    },
    {
      title: "Updated At",
      dataIndex: "updatedAt",
      render: (val) => <span>{moment(val).format("DD-MM-YYYY HH:mm:ss")}</span>,
    },
    {
      title: "Action",
      fixed: "right",
      width: 150,
      render: (_, data) => (
        <Space size="middle">
          <a onClick={() => onEdit(data.id)}>Edit</a>
          <Popconfirm
            title="Chắc chắn xóa?"
            onConfirm={() => onDelete(data.id)}
          >
            <a className="text-danger">Delete</a>
          </Popconfirm>
        </Space>
      ),
    },
  ];

  return (
    <>
      <div
        style={{
          display: "flex",
          justifyContent: "flex-end",
        }}
      >
        <Button type="primary" icon={<PlusOutlined />} onClick={onAdd}>
          Add
        </Button>
      </div>

      <Table
        dataSource={users}
        columns={COLUMNS}
        rowKey="id"
        style={{ marginTop: 8 }}
        scroll={{ x: 1600 }}
      />
      <Modal
        title={`${isEdit ? "Edit" : "Add"} User`}
        open={isModalOpen}
        footer={null}
        closeIcon={
          <div onClick={hideModal}>
            <CloseOutlined />
          </div>
        }
      >
        <Form
          ref={formRef}
          onFinish={onFinish}
          layout="vertical"
          className="row-col"
          style={{ marginTop: 12 }}
        >
          <Form.Item
            className="username"
            label="Username"
            name="username"
            rules={[
              {
                required: true,
                message: "Please input username!",
              },
            ]}
          >
            <Input
              placeholder="Username"
              onChange={(txt) => onChangeText("username", txt)}
              value={user.username}
              disabled={isEdit}
            />
          </Form.Item>
          <Form.Item
            className="password"
            label="Password"
            name="password"
            rules={[
              {
                required: true,
                message: "Please input password!",
              },
            ]}
          >
            <Input
              placeholder="Password"
              onChange={(txt) => onChangeText("password", txt)}
              value={user.password}
              type="password"
              disabled={isEdit}
            />
          </Form.Item>
          <Form.Item
            className="email"
            label="Email"
            name="email"
            rules={[
              {
                type: "email",
                message: "Invalid email address!",
              },
              {
                required: true,
                message: "Please input email!",
              },
            ]}
          >
            <Input
              placeholder="Email"
              onChange={(txt) => onChangeText("email", txt)}
              value={user.email}
              disabled={isEdit}
            />
          </Form.Item>
          <Form.Item className="fullName" label="FullName" name="fullName">
            <Input
              placeholder="FullName"
              onChange={(txt) => onChangeText("fullName", txt)}
              value={user.fullName}
            />
          </Form.Item>
          <Form.Item className="phone" label="Phone" name="phone">
            <Input
              placeholder="Phone"
              onChange={(txt) => onChangeText("phone", txt)}
              value={user.phone}
            />
          </Form.Item>
          <Form.Item className="dob" label="Dob" name="dob">
            <DatePicker
              format={"DD-MM-YYYY"}
              onChange={(txt) => onChangeText("dob", txt)}
              style={{ width: "100%" }}
              defaultValue={moment()}
              value={user.dob ? moment(user.dob) : moment()}
            />
          </Form.Item>
          <Form.Item className="gender" label="Gender" name="gender">
            <Select
              defaultValue={`${user.gender}` === "1" ? "1" : "0"}
              onChange={(txt) => onChangeText("gender", txt)}
              style={{ width: "100%" }}
              value={`${user.gender}` === "1" ? "Nam" : "Nữ"}
            >
              <Option value="1">Nam</Option>
              <Option value="0">Nữ</Option>
            </Select>
          </Form.Item>
          <Form.Item className="address" label="Address" name="address">
            <Input
              placeholder="Address"
              onChange={(txt) => onChangeText("address", txt)}
              value={user.address}
            />
          </Form.Item>
          <Form.Item className="role" label="Role" name="role">
            <Select
              defaultValue={user.role ?? "staff"}
              onChange={(txt) => onChangeText("role", txt)}
              style={{ width: "100%" }}
              value={user.role}
            >
              <Option value="admin">Admin</Option>
              <Option value="manager">Manager</Option>
              <Option value="staff">Staff</Option>
              <Option value="customer">Customer</Option>
            </Select>
          </Form.Item>
          <Form.Item
            style={{
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            <Button onClick={hideModal}>Cancel</Button>
            <Button type="primary" htmlType="submit" style={{ marginLeft: 12 }}>
              {isEdit ? "Update" : "Add"}
            </Button>
          </Form.Item>
        </Form>
      </Modal>
      {contextHolder}
    </>
  );
};

export default Users;
