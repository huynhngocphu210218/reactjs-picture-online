import { Table, Popconfirm } from "antd";
import React, { useEffect, useState, useRef } from "react";
import API from "../../utils/API";
import { notification, Button, Space, Modal, Form, Input } from "antd";
import { PlusOutlined, CloseOutlined } from "@ant-design/icons";
import moment from "moment";

const Categories = () => {
  const formRef = useRef(null);
  const [categories, setCategories] = useState([]);
  const [notify, contextHolder] = notification.useNotification();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [category, setCategory] = useState({});

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    console.log("kkkkk isModalOpen isEdit", isModalOpen, isEdit);
    if (isModalOpen && isEdit) {
      formRef.current?.setFieldsValue(category);
    } else {
      formRef.current?.setFieldsValue({ name: "", note: "" });
    }
  }, [isModalOpen, isEdit]);

  const getData = async () => {
    const rs = await API.get("category");
    if (rs.code === 200) {
      setCategories(rs.data);
    } else {
      notify.error({
        message: `Load categories failed!`,
        description: rs.status,
        placement: "topRight",
      });
    }
    console.log("kkkkk categories", rs);
  };

  const showModal = () => {
    setIsModalOpen(true);
  };

  const hideModal = () => {
    setIsModalOpen(false);
  };

  const onFinish = async () => {
    console.log("kkkkk category", category);
    if (isEdit) {
      const rs = await API.patch(`category/${category.id}`, category);
      console.log("kkkkk update category", rs);
      if (rs.code === 200) {
        setIsModalOpen(false);
        getData();
      } else {
        notify.error({
          message: `Lỗi sửa danh mục!`,
          description: rs.status,
          placement: "topRight",
        });
      }
    } else {
      const rs = await API.post("category", category);
      console.log("kkkkk add category", rs);
      if (rs.code === 200) {
        setIsModalOpen(false);
        getData();
      } else {
        notify.error({
          message: `Lỗi thêm danh mục!`,
          description: rs.status,
          placement: "topRight",
        });
      }
    }
  };

  const onChangeText = (key, e) => {
    console.log("kkkkk ", e.target.value);
    setCategory({ ...category, [key]: e.target.value });
  };

  const onAdd = () => {
    setIsEdit(false);
    showModal();
  };

  const onEdit = (id) => {
    const _categ = categories.find((k) => k.id === id);
    setIsModalOpen(true);
    setIsEdit(true);
    setCategory(_categ);
    console.log("kkkkk _categ", _categ);
  };

  const onDelete = async (id) => {
    const rs = await API.delete(`category/${id}`);
    console.log("kkkkk delete category", rs);
    if (rs.code === 200) {
      notify.success({
        message: `Xóa danh mục thành công!`,
        placement: "topRight",
      });
      getData();
    } else {
      notify.error({
        message: `Lỗi xóa danh mục!`,
        description: rs.status,
        placement: "topRight",
      });
    }
  };

  const COLUMNS = [
    {
      title: "Name",
      dataIndex: "name",
    },
    {
      title: "Note",
      dataIndex: "note",
    },
    {
      title: "Created At",
      dataIndex: "createdAt",
      render: (val) => <span>{moment(val).format("DD-MM-YYYY HH:mm:ss")}</span>,
    },
    {
      title: "Updated At",
      dataIndex: "updatedAt",
      render: (val) => <span>{moment(val).format("DD-MM-YYYY HH:mm:ss")}</span>,
    },
    {
      title: "Action",
      render: (_, data) => (
        <Space size="middle">
          <a onClick={() => onEdit(data.id)}>Edit</a>
          <Popconfirm
            title="Chắc chắn xóa?"
            onConfirm={() => onDelete(data.id)}
          >
            <a className="text-danger">Delete</a>
          </Popconfirm>
        </Space>
      ),
    },
  ];

  return (
    <>
      <div
        style={{
          display: "flex",
          justifyContent: "flex-end",
        }}
      >
        <Button type="primary" icon={<PlusOutlined />} onClick={onAdd}>
          Add
        </Button>
      </div>

      <Table
        dataSource={categories}
        columns={COLUMNS}
        rowKey="id"
        style={{ marginTop: 8 }}
      />
      <Modal
        title={`${isEdit ? "Edit" : "Add"} Category`}
        open={isModalOpen}
        footer={null}
        closeIcon={
          <div onClick={hideModal}>
            <CloseOutlined />
          </div>
        }
      >
        <Form
          ref={formRef}
          onFinish={onFinish}
          layout="vertical"
          className="row-col"
          style={{ marginTop: 12 }}
        >
          <Form.Item
            className="name"
            label="Category name"
            name="name"
            rules={[
              {
                required: true,
                message: "Please input category name!",
              },
            ]}
          >
            <Input
              placeholder="Category name"
              onChange={(txt) => onChangeText("name", txt)}
            />
          </Form.Item>
          <Form.Item className="note" label="Note" name="note">
            <Input
              placeholder="Note"
              onChange={(txt) => onChangeText("note", txt)}
            />
          </Form.Item>
          <Form.Item
            style={{
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            <Button onClick={hideModal}>Cancel</Button>
            <Button type="primary" htmlType="submit" style={{ marginLeft: 12 }}>
              {isEdit ? "Update" : "Add"}
            </Button>
          </Form.Item>
        </Form>
      </Modal>
      {contextHolder}
    </>
  );
};

export default Categories;
