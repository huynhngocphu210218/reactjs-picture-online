import {
  Table,
  Space,
  Popconfirm,
  notification,
  Button,
  Modal,
  Form,
  Input,
  Select,
} from "antd";
import moment from "moment";
import { useEffect, useRef, useState } from "react";
import API from "../../utils/API";
import { CloseOutlined, PlusOutlined } from "@ant-design/icons";
import { useHistory } from "react-router-dom";
import { resetObject } from "../../utils/Common";
const { Option } = Select;
const { TextArea } = Input;

const Products = () => {
  const formRef = useRef(null);
  const [products, setProducts] = useState([]);
  const [categories, setCategories] = useState([]);
  const [brands, setBrands] = useState([]);
  const [product, setProduct] = useState([]);
  const [isEdit, setIsEdit] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [notify, contextHolder] = notification.useNotification();
  const history = useHistory();

  useEffect(() => {
    getData();
    getCategories();
    getBrands();
  }, []);

  useEffect(() => {
    console.log("kkkkk products", products);
  }, [products]);

  useEffect(() => {
    console.log("kkkkk isModalOpen isEdit", isModalOpen, isEdit);
    if (isModalOpen && isEdit) {
      console.log("kkkkk product", product);
      if (product.category)
        product.category = product.category?.id || product.category;
      if (product.brand) product.brand = product.brand?.id || product.brand.id;
      formRef.current?.setFieldsValue(product);
    } else {
      formRef.current?.setFieldsValue({ ...resetObject(product) });
    }
  }, [isModalOpen, isEdit]);

  const getData = async () => {
    const rs = await API.get("product");
    if (rs.code === 200) {
      setProducts(rs.data);
    } else {
      notify.error({
        message: `Load products failed!`,
        description: rs.status,
        placement: "topRight",
      });
    }
  };

  const getCategories = async () => {
    const rs = await API.get("category");
    console.log("kkkkk get categories", rs);
    if (rs.code === 200) {
      setCategories(rs.data);
    } else {
      notify.error({
        message: `Load categories failed!`,
        description: rs.status,
        placement: "topRight",
      });
    }
  };

  const getBrands = async () => {
    const rs = await API.get("brand");
    console.log("kkkkk get brands", rs);
    if (rs.code === 200) {
      setBrands(rs.data);
    } else {
      notify.error({
        message: `Load brands failed!`,
        description: rs.status,
        placement: "topRight",
      });
    }
  };

  const onDetail = (id) => {
    history.push(`/products/${id}`);
  };

  const onEdit = (id) => {
    const _product = products.find((k) => k.id === id);
    setIsModalOpen(true);
    setIsEdit(true);
    setProduct({ ..._product });
    console.log("kkkkk _product", _product);
  };

  const onDelete = async (id) => {
    const rs = await API.delete(`product/${id}`);
    console.log("kkkkk delete product", rs);
    if (rs.code === 200) {
      notify.success({
        message: `Xóa sản phẩm thành công!`,
        placement: "topRight",
      });
      getData();
    } else {
      notify.error({
        message: `Lỗi xóa sản phẩm!`,
        description: rs.status,
        placement: "topRight",
      });
    }
  };

  const showModal = () => {
    setIsModalOpen(true);
  };

  const hideModal = () => {
    setIsModalOpen(false);
  };

  const onChangeText = (key, e) => {
    console.log("kkkkk ", e?.target?.value || e);
    setProduct({ ...product, [key]: e?.target?.value || e });
  };

  const onAdd = () => {
    setIsEdit(false);
    showModal();
  };

  const onFinish = async () => {
    console.log("kkkkk product", product);
    if (isEdit) {
      const rs = await API.patch(`product/${product.id}`, product);
      console.log("kkkkk update product", rs);
      if (rs.code === 200) {
        setIsModalOpen(false);
        getData();
      } else {
        notify.error({
          message: `Lỗi sửa sản phẩm!`,
          description: rs.status,
          placement: "topRight",
        });
      }
    } else {
      const rs = await API.post("product", product);
      console.log("kkkkk add product", rs);
      if (rs.code === 200) {
        setIsModalOpen(false);
        getData();
      } else {
        notify.error({
          message: `Lỗi thêm sản phẩm!`,
          description: rs.status,
          placement: "topRight",
        });
      }
    }
  };

  const COLUMNS = [
    {
      title: "Name",
      dataIndex: "name",
      // fixed: "left",
      //   width: 150,
    },
    {
      title: "Category",
      dataIndex: "category",
      render: (val) => <span>{val?.name}</span>,
    },
    {
      title: "Description",
      dataIndex: "description",
      render: (val) => {
        const arr = val?.split("\n") || [];
        return arr.map((it) => (
          <span>
            {it}
            <br />
          </span>
        ));
      },
    },
    {
      title: "Created At",
      dataIndex: "createdAt",
      width: 150,
      render: (val) => <span>{moment(val).format("DD-MM-YYYY HH:mm:ss")}</span>,
    },
    {
      title: "Updated At",
      dataIndex: "updatedAt",
      width: 150,
      render: (val) => <span>{moment(val).format("DD-MM-YYYY HH:mm:ss")}</span>,
    },
    {
      title: "Action",
      width: 200,
      render: (m, data) => (
        <Space size="middle">
          <a onClick={() => onDetail(data.id)}>Details</a>
          <a onClick={() => onEdit(data.id)}>Edit</a>
          <Popconfirm
            title="Chắc chắn xóa?"
            onConfirm={() => onDelete(data.id)}
          >
            <a className="text-danger">Delete</a>
          </Popconfirm>
        </Space>
      ),
    },
  ];

  return (
    <>
      <div
        style={{
          display: "flex",
          justifyContent: "flex-end",
        }}
      >
        <Button type="primary" icon={<PlusOutlined />} onClick={onAdd}>
          Add
        </Button>
      </div>
      <Table
        dataSource={products}
        columns={COLUMNS}
        rowKey="id"
        style={{ marginTop: 8 }}
      />
      <Modal
        title={`${isEdit ? "Edit" : "Add"} Product`}
        open={isModalOpen}
        footer={null}
        closeIcon={
          <div onClick={hideModal}>
            <CloseOutlined />
          </div>
        }
      >
        <Form
          ref={formRef}
          onFinish={onFinish}
          layout="vertical"
          className="row-col"
          style={{ marginTop: 12 }}
        >
          <Form.Item
            className="name"
            label="Name"
            name="name"
            rules={[
              {
                required: true,
                message: "Please input name!",
              },
            ]}
          >
            <Input
              placeholder="Name"
              onChange={(txt) => onChangeText("name", txt)}
              value={product.name}
            />
          </Form.Item>
          <Form.Item className="category" label="Category" name="category">
            <Select
              onChange={(txt) => onChangeText("category", txt)}
              style={{ width: "100%" }}
              value={`${product.category}`}
            >
              {categories.map((c) => {
                return (
                  <Option key={c.id} value={c.id}>
                    {c.name}
                  </Option>
                );
              })}
            </Select>
          </Form.Item>

          <Form.Item
            className="description"
            label="Description"
            name="description"
          >
            <TextArea
              placeholder="Description"
              onChange={(txt) => onChangeText("description", txt)}
              value={product.description}
            />
          </Form.Item>

          <Form.Item
            style={{
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            <Button onClick={hideModal}>Cancel</Button>
            <Button type="primary" htmlType="submit" style={{ marginLeft: 12 }}>
              {isEdit ? "Update" : "Add"}
            </Button>
          </Form.Item>
        </Form>
      </Modal>
      {contextHolder}
    </>
  );
};

export default Products;
