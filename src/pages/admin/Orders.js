import { Table, Popconfirm } from "antd";
import { useEffect, useState, useRef } from "react";
import API from "../../utils/API";
import { notification, Button, Space, Modal, Form, Input } from "antd";
import { PlusOutlined, CloseOutlined } from "@ant-design/icons";
import moment from "moment";

const Orders = () => {
  const formRef = useRef(null);
  const [orders, setOrders] = useState([]);
  const [notify, contextHolder] = notification.useNotification();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [order, setOrder] = useState({});

  useEffect(() => {
    getData();
  }, []); 

  useEffect(() => {
    console.log("kkkkk isModalOpen isEdit", isModalOpen, isEdit);
    if (isModalOpen && isEdit) {
      formRef.current?.setFieldsValue(order);
    } else {
      formRef.current?.setFieldsValue({ order: "", note: "" });
    }
  }, [isModalOpen, isEdit]);

  const getData = async () => {
    const rs = await API.get("order");
    if (rs.code === 200) {
      setOrders(rs.data);
    } else {
      notify.error({
        message: `Load orders failed!`,
        description: rs.status,
        placement: "topRight",
      });
    }
    console.log("kkkkk orders", rs);
  };

  const showModal = () => {
    setIsModalOpen(true);
  };

  const hideModal = () => {
    setIsModalOpen(false);
  };

  const onFinish = async () => {
    console.log("kkkkk order", order);
    if (isEdit) {
      const rs = await API.patch(`order/${order.id}`, order);
      console.log("kkkkk update order", rs);
      if (rs.code === 200) {
        setIsModalOpen(false);
        getData();
      } else {
        notify.error({
          message: `Lỗi sửa kích thước!`,
          description: rs.status,
          placement: "topRight",
        });
      }
    } else {
      const rs = await API.post("order", order);
      console.log("kkkkk add order", rs);
      if (rs.code === 200) {
        setIsModalOpen(false);
        getData();
      } else {
        notify.error({
          message: `Lỗi thêm kích thước!`,
          description: rs.status,
          placement: "topRight",
        });
      }
    }
  };

  const onChangeText = (key, e) => {
    console.log("kkkkk ", e.target.value);
    setOrder({ ...order, [key]: e.target.value });
  };

  const onAdd = () => {
    setIsEdit(false);
    showModal();
  };

  const onEdit = (id) => {
    const _size = orders.find((k) => k.id === id);
    setIsModalOpen(true);
    setIsEdit(true);
    setOrder(_size);
    console.log("kkkkk _size", _size);
  };

  const onDelete = async (id) => {
    const rs = await API.delete(`order/${id}`);
    console.log("kkkkk delete order", rs);
    if (rs.code === 200) {
      notify.success({
        message: `Xóa kích thước thành công!`,
        placement: "topRight",
      });
      getData();
    } else {
      notify.error({
        message: `Lỗi xóa kích thước!`,
        description: rs.status,
        placement: "topRight",
      });
    }
  };

  const COLUMNS = [
    {
      title: "# ID",
      dataIndex: "transaction",
      render: (val) => <span>{val.transactionId}</span>,
    },
    {
      title: "Amount",
      dataIndex: "transaction",
      render: (val) => <span>{val.amount.toLocaleString()}₫</span>,
    },
    {
      title: "Status",
      dataIndex: "status",
    },
    {
      title: "Products",
      dataIndex: "details",
      render: (products) => {
        return products.map((p) => (
          <span>
            {p.name +
              `${p.size ? ` - ${p.size}` : ""}`}
            <br />
            <span style={{ marginLeft: 20 }}>{`${p.price.toLocaleString()}₫ x ${
              p.quantity
            }`}</span>
            <br />
          </span>
        ));
      },
    },
    {
      title: "Created At",
      dataIndex: "createdAt",
      render: (val) => <span>{moment(val).format("DD-MM-YYYY HH:mm:ss")}</span>,
    },
    {
      title: "Action",
      render: (_, data) => (
        <Space size="middle">
          <a onClick={() => onEdit(data.id)}>Edit</a>
          {/* <Popconfirm
            title="Chắc chắn xóa?"
            onConfirm={() => onDelete(data.id)}
          >
            <a className="text-danger">Delete</a>
          </Popconfirm> */}
        </Space>
      ),
    },
  ];

  return (
    <>
      <div
        style={{
          display: "flex",
          justifyContent: "flex-end",
        }}
      >
        {/* <Button type="primary" icon={<PlusOutlined />} onClick={onAdd}>
          Add
        </Button> */}
      </div>

      <Table
        dataSource={orders}
        columns={COLUMNS}
        rowKey="id"
        style={{ marginTop: 8 }}
      />
      <Modal
        title={`${isEdit ? "Edit" : "Add"} Size`}
        open={isModalOpen}
        footer={null}
        closeIcon={
          <div onClick={hideModal}>
            <CloseOutlined />
          </div>
        }
      >
        <Form
          ref={formRef}
          onFinish={onFinish}
          layout="vertical"
          className="row-col"
          style={{ marginTop: 12 }}
        >
          <Form.Item
            className="order"
            label="Size"
            name="order"
            rules={[
              {
                required: true,
                message: "Please input order!",
              },
            ]}
          >
            <Input
              placeholder="Size"
              onChange={(txt) => onChangeText("order", txt)}
              value={order.size}
            />
          </Form.Item>
          <Form.Item className="note" label="Note" name="note">
            <Input
              placeholder="Note"
              onChange={(txt) => onChangeText("note", txt)}
              value={order.note}
            />
          </Form.Item>
          <Form.Item
            style={{
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            <Button onClick={hideModal}>Cancel</Button>
            <Button type="primary" htmlType="submit" style={{ marginLeft: 12 }}>
              {isEdit ? "Update" : "Add"}
            </Button>
          </Form.Item>
        </Form>
      </Modal>
      {contextHolder}
    </>
  );
};

export default Orders;
