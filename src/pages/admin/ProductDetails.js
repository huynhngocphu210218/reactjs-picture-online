import {
  Table,
  Space,
  Popconfirm,
  notification,
  Button,
  Modal,
  Form,
  Input,
  Select,
  InputNumber,
  Image,
} from "antd";
import moment from "moment";
import { useEffect, useRef, useState } from "react";
import API from "../../utils/API";
import {
  CloseOutlined,
  PlusOutlined,
  EyeOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import { useParams } from "react-router-dom";
import UploadImages from "../../components/ui/UploadImages";
import { SERVER_BASE_URL } from "../../utils/Constants";
import { getBase64, resetObject, uriToUploadFile } from "../../utils/Common";
import ImageList from "../../components/ui/ImageList";

const { Option } = Select;
const { TextArea } = Input;

const DetailDetails = () => {
  const { id } = useParams();
  const formRef = useRef(null);
  const uploadRef = useRef();
  const deletedRef = useRef();
  const [details, setDetails] = useState([]);
  const [images, setImages] = useState([]);
  const [colors, setColors] = useState([]);
  const [sizes, setSizes] = useState([]);
  const [detail, setDetail] = useState({});
  const [isEdit, setIsEdit] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [notify, contextHolder] = notification.useNotification();

  useEffect(() => {
    getData();
    getColors();
    getSizes();
  }, []);

  useEffect(() => {
    if (isModalOpen && isEdit) {
      resetId(detail);
      formRef.current?.setFieldsValue(detail);
    } else {
      formRef.current?.setFieldsValue({ ...resetObject(detail) });
    }
  }, [isModalOpen, isEdit]);

  const getData = async () => {
    const rs = await API.get(`product/${id}/details`);
    console.log("kkkkk get details", rs);
    if (rs.code === 200) {
      setDetails(rs.data);
    } else {
      notify.error({
        message: `Load product details failed!`,
        description: rs.status,
        placement: "topRight",
      });
    }
  };

  const getColors = async () => {
    const rs = await API.get("color");
    console.log("kkkkk get colors", rs);
    if (rs.code === 200) {
      setColors(rs.data);
    }
  };

  const getSizes = async () => {
    const rs = await API.get("size");
    console.log("kkkkk get sizes", rs);
    if (rs.code === 200) {
      setSizes(rs.data);
    }
  };

  const resetId = (_detail) => {
    _detail.product = _detail.product?.id || _detail.product;
    _detail.size = _detail.size?.id || _detail.size;
  };

  const onEdit = async (id) => {
    const _detail = details.find((k) => k.id === id);
    uploadRef.current?.clear();
    setImages(_detail.images);
    setIsModalOpen(true);
    setIsEdit(true);
    setDetail({ ..._detail });
    console.log("kkkkk _detail", _detail);
  };

  const onDelete = async (id) => {
    const rs = await API.delete(`product/details/${id}`);
    console.log("kkkkk delete detail", rs);
    if (rs.code === 200) {
      notify.success({
        message: `Xóa sản phẩm chi tiết thành công!`,
        placement: "topRight",
      });
      getData();
    } else {
      notify.error({
        message: `Lỗi xóa sản phẩm chi tiết!`,
        description: rs.status,
        placement: "topRight",
      });
    }
  };

  const showModal = () => {
    setIsModalOpen(true);
  };

  const hideModal = () => {
    setIsModalOpen(false);
  };

  const onChange = (key, e) => {
    console.log("kkkkk ", e?.target?.value || e);
    setDetail({ ...detail, [key]: e?.target?.value || e });
  };

  const onAdd = () => {
    setIsEdit(false);
    showModal();
    uploadRef.current?.clear();
    setImages([]);
  };

  const onFinish = async () => {
    console.log("kkkkk onFinish1", detail);
    resetId(detail);
    const images = [];
    for (let idx = 0; idx < detail.images.length; idx++) {
      const _img = detail.images[idx];
      const base64 = await getBase64(_img.originFileObj);
      images.push({
        base64,
        type: _img.type,
        size: _img.size,
      });
    }
    detail.images = images;
    detail.color = 1;
    detail.deleteImages = deletedRef.current?.deletedList();
    console.log("kkkkk onFinish2", detail);
    if (isEdit) {
      const rs = await API.patch(`product/details/${detail.id}`, detail);
      console.log("kkkkk update detail", rs);
      if (rs.code === 200) {
        setIsModalOpen(false);
        getData();
      } else {
        notify.error({
          message: `Lỗi sửa sản phẩm chi tiết!`,
          description: rs.status,
          placement: "topRight",
        });
      }
    } else {
      const rs = await API.post(`product/${id}/details`, detail); // "product/" + id + "/details"   = product/2/details
      console.log("kkkkk add detail", rs);
      if (rs.code === 200) {
        setIsModalOpen(false);
        getData();
      } else {
        notify.error({
          message: `Lỗi thêm sản phẩm!`,
          description: rs.status,
          placement: "topRight",
        });
      }
    }
  };

  const COLUMNS = [
    // {
    //   title: "Name",
    //   dataIndex: "product",
    //   render: (p) => <span>{p.name}</span>,
    // },
    {
      title: "Size",
      dataIndex: "size",
      render: (s) => <span>{s?.size}</span>,
    },
    {
      title: "Price",
      dataIndex: "price",
    },
    {
      title: "Quantity",
      dataIndex: "quantity",
    },
    {
      title: "Description",
      dataIndex: "description",
    },
    {
      title: "Images",
      dataIndex: "images",
      render: (imgs) => (
        <div>
          {imgs.map((i) => {
            return (
              <Image
                key={i.id}
                style={{ marginLeft: 5, width: 50 }}
                src={`${SERVER_BASE_URL}/api/images?filename=${i.name}`}
                preview={{
                  mask: <EyeOutlined />,
                }}
              />
            );
          })}
        </div>
      ),
    },
    // {
    //   title: "Created At",
    //   dataIndex: "createdAt",
    //   width: 150,
    //   render: (val) => <span>{moment(val).format("DD-MM-YYYY HH:mm:ss")}</span>,
    // },
    {
      title: "Updated At",
      dataIndex: "updatedAt",
      width: 150,
      render: (val) => (
        <div style={{ textAlign: "center" }}>
          <span>{moment(val).format("DD-MM-YYYY HH:mm:ss")}</span>
        </div>
      ),
    },
    {
      title: "Action",
      width: 200,
      render: (m, data) => (
        <Space size="middle">
          <a onClick={() => onEdit(data.id)}>Edit</a>
          <Popconfirm
            title="Chắc chắn xóa?"
            onConfirm={() => onDelete(data.id)}
          >
            <a className="text-danger">Delete</a>
          </Popconfirm>
        </Space>
      ),
    },
  ];

  return (
    <>
      <div
        style={{
          display: "flex",
          justifyContent: "flex-end",
        }}
      >
        <Button type="primary" icon={<PlusOutlined />} onClick={onAdd}>
          Add
        </Button>
      </div>
      <Table
        dataSource={details}
        columns={COLUMNS}
        rowKey="id"
        style={{ marginTop: 8 }}
      />
      <Modal
        title={`${isEdit ? "Edit" : "Add"} product detail`}
        open={isModalOpen}
        footer={null}
        closeIcon={
          <div onClick={hideModal}>
            <CloseOutlined />
          </div>
        }
      >
        <Form
          ref={formRef}
          onFinish={onFinish}
          layout="vertical"
          className="row-col"
          style={{ marginTop: 12 }}
        >
          <Form.Item
            className="size"
            label="Size"
            name="size"
            rules={[
              {
                required: true,
                message: "Please input size!",
              },
            ]}
          >
            <Select
              onChange={(txt) => onChange("size", txt)}
              style={{ width: "100%" }}
              value={`${detail.size}`}
            >
              {sizes.map((c) => {
                return (
                  <Option key={c.id} value={c.id}>
                    {c.size}
                  </Option>
                );
              })}
            </Select>
          </Form.Item>

          <Form.Item
            className="price"
            label="Price"
            name="price"
            rules={[
              {
                required: true,
                message: "Please input price!",
              },
            ]}
          >
            <InputNumber
              style={{ width: "100%" }}
              placeholder="Price"
              onChange={(txt) => onChange("price", txt)}
              value={detail.price}
              min={1000}
            />
          </Form.Item>
          <Form.Item
            className="quantity"
            label="Quantity"
            name="quantity"
            rules={[
              {
                required: true,
                message: "Please input quantity!",
              },
            ]}
          >
            <InputNumber
              style={{ width: "100%" }}
              placeholder="Quantity"
              onChange={(txt) => onChange("quantity", txt)}
              value={detail.quantity}
              min={1}
            />
          </Form.Item>
          <Form.Item
            className="description"
            label="Description"
            name="description"
            rules={[
              {
                required: true,
                message: "Please input description!",
              },
            ]}
          >
            <TextArea
              rows={4}
              placeholder="Description"
              onChange={(txt) => onChange("description", txt)}
              value={detail.description}
            />
          </Form.Item>
          <Form.Item className="note" label="Note" name="note">
            <Input
              placeholder="Note"
              onChange={(txt) => onChange("note", txt)}
              value={detail.note}
            />
          </Form.Item>
          <ImageList ref={deletedRef} images={images} />
          <UploadImages ref={uploadRef} onFileChange={onChange} />
          <Form.Item
            style={{
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            <Button onClick={hideModal}>Cancel</Button>
            <Button type="primary" htmlType="submit" style={{ marginLeft: 12 }}>
              {isEdit ? "Update" : "Add"}
            </Button>
          </Form.Item>
        </Form>
      </Modal>
      {contextHolder}
    </>
  );
};

export default DetailDetails;
