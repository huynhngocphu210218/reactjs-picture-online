import { Row, Col, Radio, Input, Select, Button } from "antd";
import UserStore from "../../stores/UserStore";
import { SERVER_BASE_URL } from "../../utils/Constants";
import { useLocation, useHistory } from "react-router-dom";
import API from "../../utils/API";
import { observer } from "mobx-react";
import { useState } from "react";
import { DeleteOutlined } from "@ant-design/icons";

const { TextArea } = Input;
const { Option } = Select;

const DetailProductiInCart = () => {
  const history = useHistory();
  const [value, setValue] = useState(1);
  const [paymentMethod, setPaymentMethod] = useState("CC");
  const [user, setUser] = useState({ gender: 1 });

  const onChange = (key, e) => {
    console.log("onChange", e.target?.value || e);
    if (key === "gender") {
      console.log(`checked = ${e.target.checked}`);
      setValue(e.target.value);
      setUser({ ...user, gender: e.target.value });
      return;
    }
    if (key === "paymentMethod") {
      setPaymentMethod(e);
      return;
    }
    setUser({ ...user, [key]: e.target.value });
  };

  const _goToPay = async () => {
    UserStore.setUserPayment(user);
    const payArr = UserStore.productsInCart.map((p) => {
      return {
        itemid: p.id,
        itename: p.product.name,
        itemprice: p.price,
        itemquantity: p.quantity,
      };
    });
    const payZalo = {
      store: "Bán Tranh Store",
      items: JSON.stringify(payArr),
      method: paymentMethod,
      redirectUrl: "http://localhost:3000/PaymentResult",
    };
    const rs = await API.post("/payment/zalo/link", payZalo);
    // history.push(rs.data.o)
    const winLink = window.open(rs.data.order_url, "_self");
    console.log("kkkkk zalo link", rs);
  };
  console.log("kkkkk productsInCart", UserStore.productsInCart);
  const _getImageUrl = (images) => {
    if (images?.length > 0) {
      return `${SERVER_BASE_URL}/api/images?filename=${images[0].name}`;
    }
    return "";
  };

  const onDelete = (data) => {
    const _delete = UserStore.productsInCart.filter((k) => k.id !== data.id);
    UserStore.setProductsInCart(_delete);
    console.log("delete", _delete);
  };

  return (
    <>
      {UserStore.productsInCart?.map((data) => {
        console.log("data", data);
        return (
          <Row style={{ width: 200, height: "auto", margin: 15 }}>
            <Row>
              <img src={_getImageUrl(data.images)} />
            </Row>
            <Row>
              <h2>{data.product.name}</h2>
            </Row>
            <Row>
              <h2>
                Giá:{" "}
                <b style={{ color: "red" }}>{data.price.toLocaleString()}₫</b>
              </h2>
            </Row>
            <Row>
              <h3>Số Lượng: {data.quantity}</h3>
            </Row>
            <DeleteOutlined
              onClick={() => onDelete(data)}
              style={{ fontSize: 25, marginLeft: 10 }}
            />
          </Row>
        );
      })}
      <Row>
        <Col span={5} />
        <Col
          span={14}
          style={{
            backgroundColor: "#FFFFFF",
            border: "1px solid #ddd",
            marginTop: 10,
            marginBottom: 30,
            paddingTop: 20,
            paddingBottom: 20,
          }}
        >
          <div style={{ marginLeft: 15 }}>
            <h4>Thông tin khách hàng</h4>
            <Radio.Group onChange={(e) => onChange("gender", e)} value={value}>
              <Radio value={1}>Anh</Radio>
              <Radio value={2}>Chị</Radio>
            </Radio.Group>
            <Row>
              <Col span={11}>
                <Input
                  placeholder="Họ và tên (bắt buộc)"
                  onChange={(e) => onChange("name", e)}
                />
              </Col>
              <Col span={11} style={{ marginLeft: 5 }}>
                <Input
                  placeholder="Số điện thoại (bắt buộc)"
                  onChange={(e) => onChange("phone", e)}
                />
              </Col>
            </Row>
            <h4>Địa chỉ nhận hàng</h4>
            <Row>
              <TextArea
                placeholder="Địa chỉ nhận hàng (bắt buộc)"
                style={{ width: "92.5%", marginTop: 5 }}
                onChange={(e) => onChange("address", e)}
              />
            </Row>
            <h4 style={{ marginTop: 5 }}>Yêu cầu khác (không bắt buộc)</h4>
            <TextArea
              style={{ width: "92.5%", marginTop: 5 }}
              onChange={(e) => onChange("note", e)}
            />
            <h4 style={{ marginTop: 5 }}>Phương thức thanh toán</h4>
            <Select
              defaultValue="CC"
              onChange={(e) => onChange("paymentMethod", e)}
              style={{ width: "92.5%", marginTop: 5 }}
              value={paymentMethod}
            >
              <Option value="CC">ZaloPay - Visa, Master card</Option>
              <Option value="ATM">ZaloPay - ATM</Option>
            </Select>
          </div>
          <Button
            style={{
              width: "91%",
              marginLeft: 15,
              color: "white",
              marginTop: 15,
            }}
            type="primary"
            onClick={_goToPay}
            disabled={!user.name || !user.phone || !user.address}
          >
            Đặt hàng
          </Button>
        </Col>
        <Col span={5} />
      </Row>
    </>
  );
};
export default observer(DetailProductiInCart);
