import { Row, Col, Card } from "antd";
import { useEffect, useState, useRef } from "react";
import API from "../../utils/API";
import _ from "lodash";
import { KEY_PRODUCTS_IN_CART, SERVER_BASE_URL } from "../../utils/Constants";
import { PlusOutlined, MinusOutlined } from "@ant-design/icons";
import { useLocation } from "react-router-dom";
import UserStore from "../../stores/UserStore";

const { Meta } = Card;
const Addtocart = () => {
  const [action, setAction] = useState("");
  const [counter, setCounter] = useState(1);
  const location = useLocation();
  const data = location.state.data;
  console.log("kkkkk data", data);
  const reduce = () => {
    setCounter(counter - 1 && counter != 0);
  };
  const increase = () => {
    setCounter(counter + 1);
  };
  const [products, setProducts] = useState([]);
  useEffect(() => {
    getData();
  }, []);
  const getData = async () => {
    const rs = await API.get("product/details");
    if (rs.code === 200) {
      setProducts(_.chunk(rs.data, 4));
    }
  };
  const _getImageUrl = (images) => {
    if (images?.length > 0) {
      return `${SERVER_BASE_URL}/api/images?filename=${images[0].name}`;
    }
    return "";
  };

  const addProductToCart = () => {
    let _products = UserStore.productsInCart;
    let _item = { ...data, quantity: counter };
    if (_products) {
      _products = [..._products.filter((i) => i.id !== _item.id), _item];
    } else {
      _products = [_item];
    }
    UserStore.setProductsInCart(_products);
  };

  return (
    <>
      <Row justify="center">
        <Row style={{ width: 400, height: "auto" }}>
          {
            <img
              alt="example"
              src={`${SERVER_BASE_URL}/api/images?filename=${data.images[0]?.name}`}
            />
          }
        </Row>
        <Card>
          <Meta
            title={data.product.name}
            description={data.price.toLocaleString()}
          />
          <Row>
            <button style={{ margin: 5 }} onClick={reduce}>
              <MinusOutlined />
            </button>
            <h3 style={{ margin: 5 }}>{counter}</h3>
            <button style={{ margin: 5 }} onClick={increase}>
              <PlusOutlined />
            </button>
          </Row>
          <button onClick={addProductToCart}>them</button>
        </Card>
      </Row>
    </>
  );
};

export default Addtocart;
