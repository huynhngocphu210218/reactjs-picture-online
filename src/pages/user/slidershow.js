import React from 'react';
import { Fade } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'

const fadeImages = [
  {
    url: 'https://wall.vn/wp-content/uploads/2020/04/hinh-anh-nha-trang-12.jpg',

  },
  {
    url: 'https://kientrucsuvietnam.vn/wp-content/uploads/2019/06/tranh-hoa-sen-2.jpg',

  },
  {
    url: 'https://th.bing.com/th/id/R.af2a3cb7b33a753ceb7256d711b54679?rik=krGVXSxTbM3yKw&pid=ImgRaw&r=0',

  },
];

const Slideshow = () => {
  return (
    <div className="slide-container">
      <Fade>
        {fadeImages.map((fadeImage, index) => (
          <div key={index}>
            <img style={{ width:'100%', height:400 }} src={fadeImage.url} />
            <h2>{fadeImage.caption}</h2>
          </div>
        ))}
      </Fade>
    </div>
  )
}
export default Slideshow;