import { Button, Result } from "antd";
import UserStore from "../../stores/UserStore";
import { observer } from "mobx-react";
import { useLocation } from "react-router-dom";
import { useEffect } from "react";
import API from "../../utils/API";

const PaymentResult = () => {
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const status = searchParams.get("status");

  useEffect(() => {
    paymentSuccess();
  }, [UserStore.productsInCart, UserStore.userPayment]);

  const paymentSuccess = async () => {
    if (UserStore.productsInCart.length === 0) return;
    const keys = [
      "amount",
      "discountamount",
      "appid",
      "checksum",
      "apptransid",
      "bankcode",
      "pmcid",
      "status",
    ];
    const trans = {};
    keys.forEach((key) => {
      trans[key] = searchParams.get(key);
    });
    console.log("kkkkk trans", trans);
    console.log("kkkkk productsInCart", UserStore.productsInCart);
    console.log("kkkkk userPayment", UserStore.userPayment);
    const products = UserStore.productsInCart.map((p) => {
      return {
        name: p.product.name,
        size: p.size.size,
        color: p.color.name,
        price: p.price,
        quantity: p.quantity,
      };
    });
    const data = {
      transaction: JSON.stringify(trans),
      products: JSON.stringify(products),
      shippingInfo: JSON.stringify(UserStore.userPayment),
    };
    console.log("kkkkk data", data);
    const rs = await API.post("/order", data);
    console.log("kkkkk create order", rs);
    if (rs.code === 200) {
      UserStore.setProductsInCart([]);
    }
  };

  let _view = <div />;
  if (status === "1") {
    _view = (
      <Result
        status="success"
        title="Thanh toán thành công!"
        subTitle={`${UserStore.userPayment?.gender === 1 ? "Anh" : "Chị"} ${
          UserStore.userPayment.name
        } đã thanh toán thành công, nhân viên giao hàng sẽ liên hệ thông qua số điện thoại ${
          UserStore.userPayment.phone
        }.`}
      />
    );
  } else {
    _view = (
      <Result
        status="error"
        title="Thanh toán không thành công!"
        subTitle={`Vui lòng kiểm tra và thanh toán lại.`}
      />
    );
  }

  return _view;
};
export default observer(PaymentResult);
