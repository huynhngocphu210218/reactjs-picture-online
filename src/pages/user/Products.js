import { Row, Col, Card } from "antd";
import { useEffect, useState, useRef } from "react";
import API from "../../utils/API";
import _ from "lodash";
import { SERVER_BASE_URL } from "../../utils/Constants";
import { NavLink, useHistory } from "react-router-dom";

import { Button, Form } from "antd";
import { PlusOutlined } from "@ant-design/icons";
// const [isModalOpen, setIsModalOpen] = useState(false);
// const formRef = useRef(null);
// const [isEdit, setIsEdit] = useState(false);
const { Meta } = Card;
// const showModal = () => {
//   setIsModalOpen(true);
// };
// const onAdd = () => {
//   setIsEdit(false);
//   showModal();
// };
const Products = (props) => {
  const [products, setProducts] = useState([]);
  const history = useHistory();

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const rs = await API.get("product/details");
    if (rs.code === 200) {
      setProducts(_.chunk(rs.data, 4));
      console.log("kkkkk _.chunk(DATA, 4)", _.chunk(rs.data, 4));
    }
  };

  const _getImageUrl = (images) => {
    if (images?.length > 0) {
      return `${SERVER_BASE_URL}/api/images?filename=${images[0].name}`;
    }
    return "";
  };

  const _goToDetail = (data) => {
    history.push("/Cart", { data });
  };

  const Item = ({ data }) => {
    return (
      <Col span={6} style={{ borderColor: "black", borderWidth: 1 }}>
        <Card
          hoverable
          onClick={() => _goToDetail(data)}
          style={{ width: 240, height: 400, margin: 15 }}
          cover={<img alt="example" src={_getImageUrl(data.images)} />}
        >
          <Meta title={data.product.name} description={data.price.toLocaleString()} />
        </Card>
      </Col>
    );
  };

  return (
    <>
      {products.map((group) => {
        return (
          <Row>
            {group[0] && <Item data={group[0]} />}
            {group[1] && <Item data={group[1]} />}
            {group[2] && <Item data={group[2]} />}
            {group[3] && <Item data={group[3]} />}
          </Row>
        );
      })}
    </>
  );
};

export default Products;
