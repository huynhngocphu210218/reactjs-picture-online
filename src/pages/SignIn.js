import React, { useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import {
  Layout,
  Button,
  Row,
  Col,
  Typography,
  Form,
  Input,
  Switch,
} from "antd";
import signinbg from "../assets/images/img-signin.jpg";
import { notification } from "antd";
import API from "../utils/API";
import { USER_INFO } from "../utils/Constants";

const { Title, Text } = Typography;
const { Content } = Layout;

const SignIn = () => {
  const [checked, setChecked] = useState(true);
  const [notify, contextHolder] = notification.useNotification();
  const history = useHistory();

  useEffect(() => {
    const _user = localStorage.getItem(USER_INFO);
    // if (_user) history.push("/dashboard");
    console.log("kkkkk _user", _user);
  }, []);

  const onFinish = async (values) => {
    const { username, password } = values;
    const rs = await API.post("login", { username, password });

    if (rs.code === 200) {
      history.push("/home");
      localStorage.setItem(USER_INFO, JSON.stringify(rs.data));
    } else {
      notify.error({
        message: `Login failed!`,
        description: rs.status,
        placement: "topRight",
      });
    }
    console.log("kkkkk rs", rs);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const onChange = () => {
    console.log("kkkkk checked", checked);
    setChecked(!checked);
  };

  return (
    <>
      <Layout className="layout-default layout-signin">
        <Content className="signin">
          <Row gutter={[24, 0]} justify="space-around">
            <Col
              xs={{ span: 24, offset: 0 }}
              lg={{ span: 6, offset: 2 }}
              md={{ span: 12 }}
            >
              <Title className="mb-15">Sign In</Title>
              <Text type="secondary" style={{ fontSize: 16 }}>
                Enter your username and password to sign in
              </Text>
              <Form
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                layout="vertical"
                className="row-col"
                style={{ marginTop: 12 }}
              >
                <Form.Item
                  className="username"
                  label="Username"
                  name="username"
                  rules={[
                    {
                      required: true,
                      message: "Please input your username!",
                    },
                  ]}
                >
                  <Input placeholder="Username" />
                </Form.Item>

                <Form.Item
                  className="username"
                  label="Password"
                  name="password"
                  rules={[
                    {
                      required: true,
                      message: "Please input your password!",
                    },
                  ]}
                >
                  <Input placeholder="Password" type="password" />
                </Form.Item>

                <Form.Item
                  name="remember"
                  className="aligin-center"
                  valuePropName="checked"
                >
                  <Switch checked={checked} onChange={onChange} />
                  Remember me
                </Form.Item>

                <Form.Item>
                  <Button
                    type="primary"
                    htmlType="submit"
                    style={{ width: "100%" }}
                  >
                    SIGN IN
                  </Button>
                </Form.Item>
                <p className="font-semibold text-muted">
                  Don't have an account?{" "}
                  <Link to="/sign-up" className="font-bold">
                    Sign Up
                  </Link>
                </p>
              </Form>
            </Col>
            <Col
              className="sign-img"
              style={{ padding: 12 }}
              xs={{ span: 24 }}
              lg={{ span: 12 }}
              md={{ span: 12 }}
            >
              <img src={signinbg} alt="" />
            </Col>
          </Row>
        </Content>
        {contextHolder}
      </Layout>
    </>
  );
};
export default SignIn;
