import { makeAutoObservable } from "mobx";
import { KEY_PRODUCTS_IN_CART, KEY_USER_PAYMENT } from "../utils/Constants";

class UserStore {
  productsInCart = [];
  userPayment = {};
  constructor() {
    makeAutoObservable(this);
  }

  setProductsInCart(products, skip = false) {
    this.productsInCart = products;
    console.log("kkkkk this.productsInCart", this.productsInCart);
    if (!skip) {
      if (products.length === 0) {
        localStorage.removeItem(KEY_PRODUCTS_IN_CART);
      } else {
        localStorage.setItem(
          KEY_PRODUCTS_IN_CART,
          JSON.stringify(this.productsInCart)
        );
      }
    }
  }

  loadUserStore() {
    let _products = localStorage.getItem(KEY_PRODUCTS_IN_CART);
    let _userPayment = localStorage.getItem(KEY_USER_PAYMENT);
    if (_products) {
      _products = JSON.parse(_products);
      this.productsInCart = _products;
    }
    if (_userPayment) {
      _userPayment = JSON.parse(_userPayment);
      this.userPayment = _userPayment;
    }
  }

  setUserPayment(user) {
    console.log("kkkkk setUserPayment", user);
    this.userPayment = user;
    if (Object.keys(user).length === 0) {
      localStorage.removeItem(KEY_USER_PAYMENT);
    } else {
      localStorage.setItem(KEY_USER_PAYMENT, JSON.stringify(this.userPayment));
    }
  }
}

export default new UserStore();
